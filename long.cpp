#include "long.h"

/**
 * Implementation of class Long
 * @ author Rannev Egor
 */

Long::~Long(){
    mylong.clear();
}


int Long::minlength(vector <int> v1, vector <int> v2){
    if (v1.size()>=v2.size())
        return (int)v2.size();
    else
        return (int)v1.size();
} 

int Long::maxlength(vector <int> v1, vector <int> v2){
    if (v1.size()>=v2.size())
        return (int)v1.size();
    else
        return (int)v2.size();    
}

int Long::compare_vector(vector <int> v1, vector <int> v2){
    if (v1.size()>v2.size())
        return 0;
    if (v2.size()>v1.size())
        return 1;
    int maxl=maxlength(v1,v2);
    for (int i=maxl-1; i>=0; i--){
        if (v1[i]>v2[i])
            return 0;
        if (v2[i]>v1[i])
            return 1;
    }
    return 2;
}

int operator> (Long L1, Long L2){
    bool lsign=L1.getsign(), lsign2=L2.getsign();
    if ((lsign&&lsign2&&(L1.compare_vector(L1.mylong,L2.mylong)==0))||
       ((!lsign)&&(!lsign2)&&(L1.compare_vector(L1.mylong,L2.mylong)==1))
                                                    ||(lsign&&(!lsign2)))
        return 1;
    else
    return 0;  
}

int operator== (Long L1, Long L2){
    bool lsign=L1.getsign(), lsign2=L2.getsign();
    if (((lsign&&lsign2)||((!lsign)&&(!lsign2)))&&
              (L1.compare_vector(L1.mylong,L2.mylong)==2))
        return 1;
    else
        return 0;
}

int operator>= (Long L1, Long L2){
    return ((L1>L2)||(L1==L2));
}

int operator< (Long L1, Long L2){
    return !(L1>=L2);
}

int operator<= (Long L1, Long L2){
    return !(L1>L2);
}

int operator!= (Long L1, Long L2){
    return !(L1==L2);
}

Long &Long::operator= (Long L){
    if (&L==this)
        return *this;
    mylong.clear();
    mylong=L.getvector();
    lsign=L.getsign();
    return *this;
}

vector <int> Long::getvector(){
    return mylong;
}

bool Long::getsign(){
    return lsign;
}

Long::Long(){
    lsign=true;
}

Long::Long(int x){
    if (x>=0)
        lsign=true;
    else{
        x=-x;
        lsign=false;
    }
    if (x==0)
        mylong.push_back(0);
    while (x!=0){
        mylong.push_back(x%kBase);
        x=x/kBase;
    }
}

Long::Long(vector <int> v, bool b){
    mylong=v;
    lsign=b;
}

Long operator+ (Long L1, Long L2){
    vector <int> sum;
    bool lsign2=L2.getsign(), lsign=L1.getsign();
    int over_sum=0, minl=L1.minlength(L1.mylong, L2.mylong), assist_status=0;
    int maxl=L1.maxlength(L1.mylong, L2.mylong);
    vector <int> v1, v2;
    if ((assist_status=L1.compare_vector(L1.mylong, L2.mylong))==1){
        v1=L2.mylong;
        v2=L1.mylong;
    }else{
        v1=L1.mylong;
        v2=L2.mylong;
    }
    if((lsign&&lsign2)||((!lsign)&&(!lsign2))){
        for(int i=0; i<minl; i++){
            sum.push_back((v1[i]+v2[i]+over_sum)%Long::kBase);
            over_sum=(v1[i]+v2[i]+over_sum)/Long::kBase;
        }
        for(int i=minl; i<maxl; i++){
            sum.push_back((v1[i]+over_sum)%Long::kBase);
            over_sum=(v1[i]+over_sum)/Long::kBase;
        }
        if (over_sum!=0)
          sum.push_back(over_sum);
    }else{
        int z=0;
        for (int i=0; i<minl; i++){
            z=v1[i]-v2[i]-over_sum;
            over_sum=0;
            if (z<0){
                z=z+Long::kBase;
                over_sum=1;
            }
            sum.push_back(z);
        }
        for (int i=minl; i<maxl; i++){
          z=v1[i]-over_sum;
          over_sum=0;
          if (z<0){
              z=z+Long::kBase;
              over_sum=1;
          }
          sum.push_back(z);
        }
    } 
    v1.clear();
    v2.clear();
    while (sum[sum.size()-1]==0)
        sum.pop_back();
    if (sum.empty())
        sum.push_back(0);
    if (assist_status==1)
        return Long(sum,lsign2);
    else
        return Long(sum,lsign);
}

Long operator- (Long L1, Long L2){
    return (L1+Long(L2.mylong,!(L2.lsign)));
}

Long &Long::operator+= (Long L){
    (*this)=(*this)+L;
    return *this;
}

Long &Long::operator*= (Long L){
    (*this)=(*this)*L;
    return *this;
}

Long &Long::operator-= (Long L){
    (*this)=(*this)-L;
    return *this;
}

Long &Long::operator/= (Long L){
    (*this)=(*this)/L;
    return *this;
}

Long &Long::operator%= (Long L){
    (*this)=(*this)%L;
    return *this;
}

Long Long::operator++(){
    (*this)+=1;
    return *this;
}

Long Long::operator--(){
    (*this)-=1;
    return *this;
}

Long Long::operator++(int){
    Long tmp(*this);
    ++(*this);
    return tmp;
}

Long Long::operator--(int){
    Long tmp(*this);
    --(*this);
    return tmp;
}

Long operator* (Long L1, Long L2){
    vector <int> mymul((L1.mylong.size()+L2.mylong.size()+1), 0);
    int over_mul=0,tmp_save=0, mulsize=((int)mymul.size()-1);
    for (int i=0; i<(int)L1.mylong.size(); i++){
        for (int j=0; j<(int)L2.mylong.size(); j++){
            tmp_save=mymul[i+j];
            mymul[i+j]=(mymul[i+j]+L1.mylong[i]*L2.mylong[j]+over_mul)
                                                          %Long::kBase;
            over_mul=(tmp_save+L1.mylong[i]*L2.mylong[j]+over_mul)/Long::kBase;
        }
        mymul[i+(int)L2.mylong.size()]=over_mul;
        over_mul=0;
    }
    while (mymul[mulsize]==0){
        mymul.pop_back();
        mulsize--;
    }
    if ((L1.lsign&&L2.lsign)||((!L1.lsign)&&(!L2.lsign)))
        return Long(mymul,true);
    else
        return Long(mymul,false);
}

pair <Long, Long> Long::divmod(Long L){
    int difl=((int)mylong.size()-(int)L.mylong.size());
    Long unsdor(L.mylong,true);
    Long q=0;
    if (L==0){
        printf("error: div on zero\n");
        exit(1);
    }
    bool dsign=true,msign=true;
    if ((!lsign)&&(!L.lsign))
        msign=false;
    if (lsign&&(!L.lsign))
        dsign=false;
    if ((!lsign)&&(L.lsign)){
        msign=false;
        dsign=false;
    }  
    if (difl<0)
        return pair<Long,Long>(0,Long(mylong,msign));
    vector <int> tempv;
    for (int i=(int)L.mylong.size(); i>0; i--)
        tempv.push_back(mylong[mylong.size()-i]);
    Long templ(tempv,true);
    tempv.clear();
    int t_low, t_high, t_cure;
    while (difl>=0){
        t_low=0;
        t_high=kBase;
        t_cure=kBase/2;
        while ((unsdor*t_cure>templ)||(unsdor*(t_cure+1)<=templ)){
            if (unsdor*t_cure>templ)
                t_high=t_cure;
            if (unsdor*(t_cure+1)<=templ)
                t_low=t_cure;
            t_cure=(t_high+t_low)/2;
        }
        q=q+t_cure;
        templ=templ-unsdor*t_cure;
        if (difl==0)
            break;
        templ=templ*kBase+mylong[--difl];
        q=q*kBase;
    }
    return pair<Long,Long>(Long(q.getvector(),dsign),
                           Long(templ.getvector(),msign));
}

Long operator/ (Long L1, Long L2){
    return L1.divmod(L2).first;
}

Long operator% (Long L1, Long L2){
    return L1.divmod(L2).second;
}

istream &operator>> (istream &O, Long &L){  
    int i=0, b=0, c, sizecontrol=0, k=1, x=false;
    stack <char> inversstream;
    c=getchar();
    if (c=='-'){
        L.lsign=false;
        c=getchar();
    }
    else
        if (c=='+')
            c=getchar();
    while (c=='0'){
        x=true;
        c=getchar();
    }
    if ((c=='\n')&&x){
        L.mylong.push_back(0);
        L.lsign=true;
        return O;
    }
    do{
        sizecontrol++;
        inversstream.push(c);
        if (sizecontrol>=100000){
            printf("error: length >= 100000\n");
            exit(1);
        }    
    } while ((c=getchar())!='\n');
    while (!inversstream.empty()){
        c=inversstream.top();
        inversstream.pop();
        if (c=='\n'){
            printf("\"-\" or \"+\" unc form of mylong");
            exit(1);
        }
        if ((c>'9')||(c<'0')){
            printf("error: unc symb in mylong\n");
            exit(1);
        }  
        i++;
        b=b+(c-'0')*k;
        k*=10;
        if (i==(Long::kBaseSize-1)){
            L.mylong.push_back(b);
            i=0;
            b=0;
            k=1;
        }      
    }
    if (i!=0)
        L.mylong.push_back(b);
    return O;
}


ostream &operator<< (ostream &O, Long L){
    int i=L.mylong.size()-1;
    int zerocontrol,k=0;
    if (i<0){
        cout<<'0';
        return O;
    }
    if (!(L.lsign))
        cout<<'-';
    printf("%d",L.mylong[i--]);
    while (i>=0){
        zerocontrol=L.mylong[i];
        if (zerocontrol==0)
            k++;
        while (zerocontrol!=0){
            zerocontrol/=10;
            k++;
        }
        for (int j=0; j<(Long::kBaseSize-k-1); j++)
            printf("0");
        printf("%d",L.mylong[i--]);
        k=0;
    }
    return O;
}

Long operator- (Long L){
    L.lsign=!L.lsign;
    return L;
}

Long operator+ (Long L){
    return L;
}

