#ifndef LONG_H_
#define LONG_H_

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <fstream>
#include <iostream>

using namespace std;

/**
 * Class of Long numbers with
 * all standart operations
 * (logical  & arithmetic)
 * @file long.h
 * @ author Rannev Egor
 */

class Long{
    static const int kBase=10000;
    static const int kBaseSize=5;
public:
    /**
     * Constructor
     * Determines sign of long
     */ 
    Long();
    
    /**
     * Copy constructor
     * @param x To copy
     */
    Long(int x);
    
    /**
     * Copy constructor (vector+sign)
     * @param v Module
     * @param b Sign
     */
    Long(vector <int> v,bool b);
    
    /**
     * Destructor
     * Delete vector
     */
    ~Long();
    
    /**
     * Output Long number in stream
     * @param O Stream to output
     * @param L Number to output
     * @return Stream to output
     */
    friend ostream &operator<< (ostream &O, Long L);
    
    /**
     * Input Long number from stream
     * @param O Stream to input
     * @param L Number to input
     * @return Stream to input
     */
    friend istream &operator>> (istream &O, Long &L);
    
    /**
     * Negation
     * @param L Number to change sign
     * @return Result of negation of L
     */
    friend Long operator- (Long L);
    
    /**
     * Unary plus
     * @param L Number
     * @return Number
     */
    friend Long operator+ (Long L);
    
    /**
     * Accessor for vector
     * @return vector
     */
    vector <int> getvector();
    
    /**
     * Accessor for sign
     * @return sign
     */
    bool getsign();
    
    /**
     * Multiplication of two numbers
     * @param L1 First number 
     * @param L2 Second number 
     * @return Result of multyply (L1*L2)
     */
    friend Long operator* (Long L1, Long L2);
    
    /**
     * Addition of two numbers
     * @param L1 First number
     * @param L2 Second number
     * @return Result of addition (L1*L2)
     */
    friend Long operator+ (Long L1, Long L2);
    
    /**
     * Subtraction of two numbers
     * @param L1 Frist number
     * @param L2 Second number
     * @return Result of subtraction (L1-L2)
     */
    friend Long operator- (Long L1, Long L2);
    
    /**
     * Division(int) of two numbers
     * @param L1 First number
     * @param L2 Second number
     * @return Result of integer divide (L1/L2)
     */
    friend Long operator/ (Long L1, Long L2);
    
    /**
     * Find the reaminder of the division
     * @param L1 First number
     * @prarm L2 Second Numbder
     * @return Remainder of divisoin (L1/L2)
     */
    friend Long operator% (Long L1, Long L2);
    
    /**
     * Postfix increment
     * @return *this
     */
    Long operator++ (int);
    
    /**
     * Prefix increment
     * @return *this
     */
    Long operator++ ();
    
    /**
     * Postfix decrement
     * @return *this
     */
    Long operator-- (int);
    
    /**
     * Prefix decrement
     * @return *this
     */
    Long operator-- ();
    
    /**
     * Assignment of Long
     * @param L To assign
     * @return *this
     */
    Long &operator= (Long L);
    
    /**
     * Addition then assignment
     * @param L Number to add
     * @return *this;
     */
    Long &operator+= (Long L);
    
    /**
     * Subtraction then assignment
     * @param L Number to sub
     * @return *this
     */
    Long &operator-= (Long L);
    
    /**
     * Multiplication then assignment
     * @param L Number to mul
     * @return *this
     */
    Long &operator*= (Long L);
    
    /** 
     * Division then assignment
     * @param L Number to div
     * @return *this
     */
    Long &operator/= (Long L);
    
    /** 
     * Mod then assignment
     * @param L number to mod
     * @return *this
     */
    Long &operator%= (Long L);
    
    /**
     * Logical more
     * @param L1 First number
     * @param L2 Second number
     * @return True if L1>L2, else False
     */
    friend int operator> (Long L1, Long L2);
    
    /**
     * Logical more or equal
     * @param L1 First number
     * @param L2 Second number
     * @return True if L1>=L2, esle Fasle
     */
    friend int operator>= (Long L1, Long L2);
    
    /** 
     * Logical less
     * @param L1 First number
     * @param L2 Second number
     * @return True if L1<L2, else False
     */
    friend int operator< (Long L1, Long L2);
    
    /**
     * Logical less or equal
     * @param L1 First number
     * @param L2 Second number
     * @return True if L1<=L2, else False
     */
    friend int operator<= (Long L1, Long L2);
    
    /** 
     * Logical equal
     * @param L1 First number
     * @param L2 Second number
     * @return True if L1==L2, else False
     */
    friend int operator== (Long L1, Long L2);
    
    /**
     * Logical not equal
     * @param L1 First number
     * @param L2 Second number
     * @return False if L1==L2, else True
     */
    friend int operator!= (Long L1, Long L2);
    
    /**
     * Find the remainder and result of the division of two Long
     * @param L Number to divide
     * @return Pair : first - reult, second - remainder of division *this by L
     */
    pair <Long,Long> divmod(Long L);
private:
    /** 
     * Compare 2 vectors as 2 unsigned numbers
     * @param v1 First vector
     * @param v2 second vector
     * @return 0 if v1>v2, 1 if v2>v1, 2 if v1=v2
     */
    int compare_vector (vector <int> v1, vector <int> v2);
    
    /**
     * Find size of more vector (as 2 unsigned numbers)
     * @param v1 First vector
     * @param v2 Second vector
     * @return size (in our base) of more vector
     */
    int maxlength(vector <int> v1, vector <int> v2);
    
    /**
     * Find size of less vector (as 2 unsigned numbers)
     * @param v1 First vector
     * @param v2 Second vector
     * @return size (in our base) of less vector
     */
    int minlength(vector <int> v1, vector <int> v2);
    bool lsign;
    vector <int> mylong;  
};

#endif //LONG_H_
