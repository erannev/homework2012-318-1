.PHONY: clean

all: long.o main.o
	g++ long.o main.o -o long

long.o: long.h long.cpp
	g++ -c long.cpp

main.o: main.cpp
	g++ -c main.cpp

testall: long-test.h long-test.cpp
	g++ long-test.h long-test.cpp -o test

clean:
	rm *.o
	rm long
	rm test
