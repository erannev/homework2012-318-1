#include <cppunit/extensions/HelperMacros.h>
#include "long.h"

/**
 * Class of test for Long class
 * @file long-test.h
 * @ author Rannev Egor
 */

class TestLong: public CPPUNIT_NS::TestFixture
{
    private:
        CPPUNIT_TEST_SUITE (TestLong);        
        CPPUNIT_TEST (testCompare1);
        CPPUNIT_TEST (testCompare2);
        CPPUNIT_TEST (testCompare3);
        CPPUNIT_TEST (testCompare4);
        CPPUNIT_TEST (testCompare5);
        CPPUNIT_TEST (testCompare6);
        CPPUNIT_TEST (testAdd1);
        CPPUNIT_TEST (testAdd2);
        CPPUNIT_TEST (testAdd3);        
        CPPUNIT_TEST (testSub1);
        CPPUNIT_TEST (testSub2);
        CPPUNIT_TEST (testSub3);
        CPPUNIT_TEST (testSub4);       
        CPPUNIT_TEST (testMul1);
        CPPUNIT_TEST (testMul2);
        CPPUNIT_TEST (testMul3);
        CPPUNIT_TEST (testMul4);
        CPPUNIT_TEST (testMul5);
        CPPUNIT_TEST (testMul6);         
        CPPUNIT_TEST (testDiv1);
        CPPUNIT_TEST (testDiv2);
        CPPUNIT_TEST (testDiv3);
        CPPUNIT_TEST (testDiv4);
        CPPUNIT_TEST (testDiv5);       
        CPPUNIT_TEST (testMod1);
        CPPUNIT_TEST (testMod2);
        CPPUNIT_TEST (testExpr1);
        CPPUNIT_TEST (testExpr2);        
        CPPUNIT_TEST_SUITE_END();        
        Long *a, *b, *c;
public:
        /**
         * Set up initial values
         */
        void setUp();
        
        /**
         * Delete values
         */
        void tearDown();
protected:
        /**
         * Test for '==' operator
         */
        void testCompare1();
        
        /** 
         * Test for '!=' operator
         */
        void testCompare2();
        
        /**
         * Test for '>' operator
         */
        void testCompare3();
        
        /**
         * Test for '<' operator
         */
        void testCompare4();
        
        /**
         * Test for '<=' operator
         */
        void testCompare5();
        
        /**
         * Test for '>=' operator
         */
        void testCompare6();
        
        /**
         * Test for overflow in addition
         */
        void testAdd1();
        
        /**
         * Test for addition with negative number
         */
        void testAdd2();
        
        /**
         * Test for a zero
         */
        void testAdd3();
        
        /**
         * Test for subtraction (2 postive numbers)
         */
        void testSub1();
        
        /** 
         * Test for subtraction (negative - positive)
         */
        void testSub2();
        
        /**
         * Test for subtraction (positive - negative)
         */
        void testSub3();
        
        /**
         * Test for a zero
         */
        void testSub4();
        
        /**
         * Test for multiplication (2 positive numbers)
         */
        void testMul1();
        
        /**
         * Test for multiplication (positive * negative)
         */
        void testMul2();
        
        /**
         * Test for multiplication ( 2 negative numbers)
         */
        void testMul3();
        
        /**
         * Test for multiplication (neagtive * positive)
         */
        void testMul4();
        
        /**
         * Test for multiplication by zero
         */
        void testMul5();
        
        /**
         * Test for increasing the size of numbers
         */
        void testMul6();
        
        /**
         * Test for division (2 positive numbers)
         */
        void testDiv1();
        
        /** 
         * Test for division (positive / negative)
         */
        void testDiv2();
        
        /**
         * Test fo division (negative / positive)
         */
        void testDiv3();
        
        /**
         * Test for division ( 2 negative numbers)
         */
        void testDiv4();
        
        /**
         * Divide test for throw when 0
         */
        void testDiv5();
        
        /**
         * Test for modulo
         */
        void testMod1();
        
        /**
         * Test for modulo (result = 0)
         */
        void testMod2();
        
        /**
         * Test for correct exprssion
         */
        void testExpr1();
        
        /**
         * Test for throw in expression
         */
        void testExpr2();
};  
