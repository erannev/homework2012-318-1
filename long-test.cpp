#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "long-test.h"

/**
 * Implementation of class of test
 * @ author Rannev Egor
 */

CPPUNIT_TEST_SUITE_REGISTRATION( TestLong );

void TestLong::setUp()
{
    a= new Long(1);
    b= new Long(1);
    c= new Long(1);
}

void TestLong::tearDown()
{
    delete a;
    delete b;
    delete c;
}

void TestLong::testAdd1()
{
    *a = 10000;
    *b = 1;
    *c = 10001;
    CPPUNIT_ASSERT(*a + *b == *c);
}

void TestLong::testAdd2()
{
    *a = -99;
    *b = 3;
    *c = -96;
    CPPUNIT_ASSERT(*a + *b == *c);
}

void TestLong::testAdd3()
{
    *a = -99999;
    *b = 99999;
    *c = 0;
    CPPUNIT_ASSERT(*a + *b == *c);
}

void TestLong::testSub1()
{
    *a = 77777;
    *b = 1;
    *c = 77776;
    CPPUNIT_ASSERT(*a - *b == *c);
}

void TestLong::testSub2()
{
    *a = -1000;
    *b =  1;
    *c = -1001;
    CPPUNIT_ASSERT(*a - *b == *c);
}

void TestLong::testSub3()
{
    *a = 1000;
    *b = -100;
    *c = 1100;
    CPPUNIT_ASSERT(*a- *b == *c);
}

void TestLong::testSub4()
{
    *a = 3333;
    *b = 3333;
    *c = 0;
    CPPUNIT_ASSERT(*a - *b == *c);
}

void TestLong::testMul1()
{
    *a = 100;
    *b = 5;
    *c = 500;
    CPPUNIT_ASSERT(*a * *b == *c);
}

void TestLong::testMul2()
{
    *a = 100;
    *b = -5;
    *c = -500;
    CPPUNIT_ASSERT(*a * *b == *c);
}
void TestLong::testMul3()
{
    *a = -100;
    *b = -5;
    *c = 500;
    CPPUNIT_ASSERT(*a * *b == *c);
}


void TestLong::testMul4()
{
    *a = -10000;
    *b = 55;
    *c = -550000;
    CPPUNIT_ASSERT(*a * *b == *c);
}

void TestLong::testMul5()
{
    *a = -12345678;
    *b = 0;
    *c = 0;
    CPPUNIT_ASSERT(*a * *b == *c);
}

void TestLong::testMul6()
{
    *a = 10000;
    *b = 10000;
    *c = 100000000;
    CPPUNIT_ASSERT(*a * *b == *c);
}


void TestLong::testDiv1()
{
    *a = 10000;
    *b = 2;
    *c = 5000;
    CPPUNIT_ASSERT(*a / *b == *c);
}

void TestLong::testDiv2()
{
    *a = 1000000;
    *b = -1000000;
    *c = -100;
    CPPUNIT_ASSERT(*a / *b == *c);
}

void TestLong::testDiv3()
{
    *a = -999999;
    *b = 9;
    *c = -111111;
    CPPUNIT_ASSERT(*a / *b == *c);
}

void TestLong::testDiv4()
{
    *a = -12345678;
    *b = -12345678;
    *c = 1;
    CPPUNIT_ASSERT(*a / *b == *c);
}

void TestLong::testDiv5()
{
    *a = 12345;
    *b = 0;
    CPPUNIT_ASSERT_THROW(*a / *b, char *);
}

void TestLong::testMod1()
{
    *a=123;
    *b=100000;
    *c=123;
    CPPUNIT_ASSERT(*a % *b == *c);
}

void TestLong::testMod1()
{
    *a=-999;
    *b=111;
    *c=0;
    CPPUNIT_ASSERT(*a % *b == *c);
}

void TestLong::testCompare1()
{
    *a = 1000;
    *b = 1000;
    CPPUNIT_ASSERT(*a == *b);
}


void TestLong::testCompare2()
{
    *a = 1000;
    *b = 10000;
    CPPUNIT_ASSERT(*a != *b);
}



void TestLong::testCompare3()
{
    *a = 15000;
    *b = 10000;
    CPPUNIT_ASSERT(*a > *b);
}


void TestLong::testCompare4()
{
    *a = -10;
    *b = 10;
    CPPUNIT_ASSERT(*a < *b);
}

void TestLong::testCompare5()
{
    *a = 13;
    *b = 13;
    CPPUNIT_ASSERT(*a <= *b);
}



void TestLong::testCompare6()
{
    *a = 33;
    *b = 22;
    CPPUNIT_ASSERT(*a >= *b);
}

void TestLong::testExpr1()
{
    *a = 10000;
    *b = -500;
    *c = 5;
    CPPUNIT_ASSERT((*a / *b) % *c + *a - *b  * 10== 15000);
}

void TestLong::testExpr2()
{
    *a = 10000;
    *b = 100;
    *c = 3;
    CPPUNIT_ASSERT_THROW( - *c / (*a - *b * 100), char *);
}




int main( int argc, char **argv)
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry
    = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
    return 0;
}
